﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : Bullet {

	public override void MoveBullet ()
	{
		transform.position += Vector3.up * Config.Me.m_playerBulletSpeed;
		if (transform.position.y > GameManager.Me.CameraRectToWorldUnits.yMax)
			Destroy (this.gameObject);
	}

}
