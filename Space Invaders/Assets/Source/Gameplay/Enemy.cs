﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[HideInInspector] public int m_xGridPlace;
	[HideInInspector] public int m_yGridPlace;

	[SerializeField] private Transform m_cannon;
	private Animator m_anim;
	private BoxCollider2D m_box;
	private float m_lastFiredTime;

	// Use this for initialization
	void Start () {

		m_anim = GetComponent<Animator> ();
		m_box = GetComponent<BoxCollider2D> ();

	}
	
	public void Die () {
	
		GameManager.Me.m_game.Score += 10;
		m_box.enabled = false;
		m_anim.SetTrigger ("death");
	
	}

	bool CanFire () {

		var playerPos = GameManager.Me.m_player.transform.position;
		return Time.time >= m_lastFiredTime + Config.Me.m_enemyFireRate &&
		transform.position.x > playerPos.x - GameManager.Me.m_player.m_halfWidth &&
		transform.position.x < playerPos.x + GameManager.Me.m_player.m_halfWidth &&
		GameManager.Me.m_game.IsBottomOfColumn (this);

	}

	void FireWeapon () {
	
		m_lastFiredTime = Time.time;
		var bullet = Instantiate<GameObject> (GameManager.Me.m_prefab_enemyBullet);
		bullet.transform.position = m_cannon.transform.position;

	}

	void Update () {
	
		if (CanFire ())
			FireWeapon ();

	}

}
