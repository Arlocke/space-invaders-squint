﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Config : MonoSingleton<Config> {

	[Header ("Gameplay Variables")]
	[Range (0.0f, 0.5f)] public float m_playerSpeed = 0.25f;
	[Range (0.0f, 1.0f)] public float m_playerBulletSpeed = 0.5f;
	[Range (0, 5)] public int m_playerStartingLives = 5;
	[Range (0.0f, 0.5f)] public float m_enemyBulletSpeed = 0.15f;
	[Range (1, 6)] public int m_enemyRows = 4;
	[Range (1, 15)] public int m_enemyColumns = 10;
	public float m_enemySpacing = 0.8f;
	public float m_enemyMoveBaseRate = 1.0f;
	[Range (0.0f, 1.0f)] public float m_enemyMoveTimeMultiplier;
	public float m_enemyMoveIncrement = 0.2f;
	public float m_enemyFireRate = 3.0f;

	/// <summary>
	/// This is a test function/method.
	/// </summary>
	public void ExampleFunction()
	{
		GameManager.Me.NewGame ();
	}

	// --------------------------------------

	#if UNITY_EDITOR
	[CustomEditor(typeof(Config))]
	public class QuestContentInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
			var config = target as Config;

			EditorGUILayout.LabelField("Debug Buttons", EditorStyles.boldLabel);

			if (GUILayout.Button("Test"))
				config.ExampleFunction();
		}
	}
	#endif

}
