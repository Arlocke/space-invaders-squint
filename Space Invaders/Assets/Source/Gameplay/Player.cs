﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public Transform m_cannon;
	private BoxCollider2D m_box;
	public float m_halfWidth;

	void Start () {
	
		var sprite = GetComponent<SpriteRenderer> ();
		m_halfWidth = sprite != null ? sprite.bounds.size.x / 2.0f : 0.0f;

	}
	
	// Update is called once per frame
	void Update () {

		UpdateMovement ();

		if (Input.GetButtonDown ("Jump"))
			FireWeapon ();

	}

	void UpdateMovement ()
	{
		/* Note:
		 * using Input.GetAxis() means that this axis can easily be used for any input we would like to use
		 * in the future, such as an onscreen joystick on a touch screen device (as the brief states), by
		 * adding to the Input Manager in the editor -- or by directly modifying the axis in code if
		 * necessary. Standard Assets already has code for a straightforward onscreen GUI joystick.
		 */

		float h = Input.GetAxis ("Horizontal");
		if (transform.position.x <= GameManager.Me.CameraRectToWorldUnits.xMin + m_halfWidth && h < 0.0f ||
			transform.position.x >= GameManager.Me.CameraRectToWorldUnits.xMax - m_halfWidth && h > 0.0f)
			h = 0.0f;

		if (h != 0.0f)
			transform.position += Vector3.right * Mathf.Clamp (h * Config.Me.m_playerSpeed, -m_halfWidth, m_halfWidth);
	}

	public void TakeDamage () {
		--GameManager.Me.m_game.Lives;
	}

	void FireWeapon () {
	
		var bullet = Instantiate<GameObject> (GameManager.Me.m_prefab_playerBullet);
		bullet.transform.position = m_cannon.transform.position;

	}
}
