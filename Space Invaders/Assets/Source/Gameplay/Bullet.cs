﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

		MoveBullet ();

	}

	public abstract void MoveBullet ();

	void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.GetComponent<Enemy> ()) {
			col.gameObject.GetComponent<Enemy> ().Die ();
		} else if (col.gameObject.GetComponent<Player> ()) {
			col.gameObject.GetComponent<Player> ().TakeDamage ();
		}
		Destroy (this.gameObject);
	}

}
