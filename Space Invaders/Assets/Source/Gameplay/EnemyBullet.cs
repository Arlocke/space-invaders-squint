﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Bullet {

	public override void MoveBullet ()
	{
		transform.position -= Vector3.up * Config.Me.m_enemyBulletSpeed;
		if (transform.position.y < GameManager.Me.CameraRectToWorldUnits.yMin)
			Destroy (this.gameObject);
	}

}
