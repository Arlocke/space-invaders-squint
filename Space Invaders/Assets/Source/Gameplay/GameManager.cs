﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager> {

	public enum Direction { left, right }

	public class Game
	{
		private int m_score;
		private int m_lives;
		private int m_currWave;

		public int Score { get { return m_score; } set { m_score = value; ViewController.Me.RefreshScore (m_score); } } // set accessor refreshes the GUI
		public int Lives { get { return m_lives; } set {
				m_lives = value;
				if (value > 0)
					ViewController.Me.RefreshLives ();
				else
					GameManager.Me.GameOver ();
			}
		}

		public Enemy[,] m_enemies; // multidimensional array of enemies. as enemies are killed, entries will become null, so null checks should be used before accessing
		public float m_currentMoveRate; // time between each movement
		public float m_lastMovement; // time index of last repositioning
		public Direction m_currentDirection; // direction of travel

		/// <summary>
		/// Constructor sets up enemy start positions and starting values
		/// </summary>
		public Game () {
			
			Score = 0;
			m_enemies = new Enemy [Config.Me.m_enemyColumns, Config.Me.m_enemyRows];
			m_currentDirection = Direction.right;
			m_lives = Config.Me.m_playerStartingLives;
			m_currWave = 0;

			GenerateNextWave ();

		}

		public void GenerateNextWave () {

			++m_currWave;
			m_currentMoveRate = Config.Me.m_enemyMoveBaseRate * Mathf.Pow (Config.Me.m_enemyMoveTimeMultiplier, m_currWave);

			for (int x = 0; x < Config.Me.m_enemyColumns; ++x) {
				for (int y = 0; y < Config.Me.m_enemyRows; ++y) {
					var newEnemy = Instantiate<GameObject> (GameManager.Me.m_prefab_Enemy);
					var pos = new Vector3 (GameManager.Me.m_startAnchor.position.x + x * Config.Me.m_enemySpacing, GameManager.Me.m_startAnchor.position.y - y * Config.Me.m_enemySpacing, 0.0f);
					newEnemy.transform.position = pos;
					m_enemies[x,y] = newEnemy.GetComponent<Enemy> ();
					m_enemies[x,y].m_xGridPlace = x;
					m_enemies[x,y].m_yGridPlace = y;
				}
			}

		}

		// Public Enemy... Fight the power!
		public Enemy GetLeadingAlien () {
			int x;
			x = m_currentDirection == Direction.left ? 0 : m_enemies.GetLength(0) - 1;
			if (x == 0) {
				for (; x < m_enemies.GetLength (0); ++x) {
					for (int y = 0; y < m_enemies.GetLength (1); ++y) {
						if (m_enemies [x, y] != null)
							return m_enemies [x, y];
					}
				}
			} else {
				for (; x >= 0; --x) {
					for (int y = 0; y < m_enemies.GetLength (1); ++y) {
						if (m_enemies [x, y] != null)
							return m_enemies [x, y];
					}
				}
			}
			return null;
		}

		public bool IsBottomOfColumn (Enemy enemy) {

			for (int i = enemy.m_yGridPlace + 1; i < m_enemies.GetLength (1); ++i) {
				if (m_enemies [enemy.m_xGridPlace, i] != null)
					return false;
			}
			return true;
		}

	}

	[Header("Prefab References")]
	public GameObject m_prefab_Enemy;
	public GameObject m_prefab_playerBullet;
	public GameObject m_prefab_enemyBullet;

	[Header("Scene References")]
	public Transform m_startAnchor;
	public Player m_player;

	// other public fields :
	public Game m_game;

	// --------------------------------------------------------------------

	void Start () {
	
		NewGame ();
	
	}

	public void NewGame () {
	
		m_game = new Game ();

	}

	public void GameOver () {
		
	}

	// any helper properties go here :

	public Rect CameraRectToWorldUnits {
		get {
			if (Camera.main.orthographic) {
				float ratio = (float) Screen.width / (float) Screen.height;
				return new Rect (new Vector2 (-Camera.main.orthographicSize * ratio, -Camera.main.orthographicSize),
					new Vector2 (Camera.main.orthographicSize * 2 * ratio, Camera.main.orthographicSize * 2));
			}
			else {
				Debug.LogError ("Camera should be set to orthographic!");
				return new Rect ();
			}
		}
	}


	// Update is called once per frame
	void Update () {

		if (m_game != null) {
			UpdateEnemies ();
		}

	}

	void UpdateEnemies () {

		if (Time.time >= m_game.m_lastMovement + m_game.m_currentMoveRate) {
			m_game.m_lastMovement = Time.time;
			Enemy leader = m_game.GetLeadingAlien ();
			if (leader != null) {
				Vector3 dir = new Vector3 ();
				switch (m_game.m_currentDirection) {
				case Direction.left:
					if (leader.transform.position.x <= m_startAnchor.transform.position.x) {
						dir = Vector3.down;
						m_game.m_currentMoveRate *= Config.Me.m_enemyMoveTimeMultiplier;
						m_game.m_currentDirection = Direction.right;
					} else {
						dir = Vector3.left;
					}
					break;
				case Direction.right:
					if (leader.transform.position.x >= -m_startAnchor.transform.position.x) {
						dir = Vector3.down;
						m_game.m_currentMoveRate *= Config.Me.m_enemyMoveTimeMultiplier;
						m_game.m_currentDirection = Direction.left;
					} else {
						dir = Vector3.right;
					}
					break;
				default:
					throw new System.ArgumentOutOfRangeException ();
				}
				foreach (var enemy in m_game.m_enemies) {
					if (enemy != null)
						enemy.transform.position += dir * Config.Me.m_enemyMoveIncrement;
				}
			} else {
				m_game.GenerateNextWave ();
			}
		}

	}

}
