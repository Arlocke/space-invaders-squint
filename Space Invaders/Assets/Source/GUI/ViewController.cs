﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewController : MonoSingleton<ViewController> {

	public Text m_score;
	public Text m_hiScore;
	public RectTransform m_livesContainer;

	private List<Image> m_lives;

	public Image m_prefab_LifeSprite;

	void Start () {
		m_lives = new List<Image> ();
		for (int i = 0; i < Config.Me.m_playerStartingLives; ++i) {
			var life = Instantiate<Image> (m_prefab_LifeSprite);
			m_lives.Add (life);
			life.transform.SetParent (m_livesContainer); // the new parent's GridLayoutGroup automatically handles placement/anchoring here.
		}
	}

	public void RefreshScore (int newScore) {

		string formattedScore = newScore.ToString ("D5");
		m_score.text = formattedScore;

	}

	public void RefreshLives ()
	{
		for (int i = 0; i < m_lives.Count; ++i) {
			m_lives [i].enabled = i < GameManager.Me.m_game.Lives;
		}
	}

}